package com.stackOverFlow;

import com.stackOverFlow.common.RestAPIVerticle;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.types.HttpEndpoint;

public class APIGateway extends RestAPIVerticle {
	
	private static final String QA_READ_SERVICE_NAME = "QAReadService";
	
	private static final int DEFAULT_PORT = 8787;

	private HttpClient client;

	@Override
	public void start(Future<Void> future) throws Exception {
		super.start();
		
		String host = config().getString("api.gateway.http.address", "localhost");
		int port = config().getInteger("api.gateway.http.port", DEFAULT_PORT);

		ServiceDiscovery discovery = ServiceDiscovery.create(vertx);
		HttpEndpoint.getClient(discovery, new JsonObject().put("name", QA_READ_SERVICE_NAME), ar -> {
			if (ar.failed()) {
				System.out.println("QA read Server Not Ready!");
			} else {
				System.out.println("Discovered the QA read Server!");
				client = ar.result();
				Router router = Router.router(vertx);

				router.get("/").handler(rc -> {
					rc.response().end("Welcome to stack over flow");
				});
				router.route().handler(BodyHandler.create());
				router.get("/questions/").handler(rc -> {
//					getQA(rc.request().uri(),new Handler<AsyncResult<Object>>() {
					getQA_2(rc.request(),new Handler<AsyncResult<Object>>() {

						@Override
						public void handle(AsyncResult<Object> result) {
							rc.response().putHeader("content-type", "application/json")
									.end(Json.encode(result.result()));
						}
					});
				});
				
				router.get("/questions/results/").handler(rc -> {
					System.out.println("APIGateway.start() URI: "+rc.request().uri());
					System.out.println("APIGateway.start() search_query: "+rc.request().getParam("search_query"));
					getQA(rc.request().uri(),new Handler<AsyncResult<Object>>() {

						@Override
						public void handle(AsyncResult<Object> result) {
							rc.response().putHeader("content-type", "application/json")
									.end(Json.encode(result.result()));
						}
					});
				});
				
				router.get("/questions/:questionId").handler(rc -> {
					System.out.println("APIGateway.start() questions/:questionId: "+rc.request().getParam("questionId"));
					getQA(rc.request().uri(),new Handler<AsyncResult<Object>>() {

						@Override
						public void handle(AsyncResult<Object> result) {
							rc.response().putHeader("content-type", "application/json")
									.end(Json.encode(result.result()));
						}
					});
				});

//				vertx.createHttpServer().requestHandler(router::accept).listen(9090);
				vertx.createHttpServer().requestHandler(router::accept).listen(port, host, res -> {
					System.out.println("APIGatewayVerticle.start() res.succeeded(): " + res.succeeded());
					if (res.succeeded()) {
						publishApiGateway(host, port);
						future.complete();
						System.out.println("API Gateway ANUPMMMMMMMMMMMMMM is running on port " + port);
					} else {
						future.fail(res.cause());
					}
				});

			}
		});

	}

	public void close() {
		client.close();
	}
	
	public void getQA_2(HttpServerRequest req,Handler<AsyncResult<Object>> handler) {
		System.out.println("APIGateway.getQA() singh uri>>>>>>>>>>>>>"+req.uri());
		client.request(req.method(),req.uri(), response -> response.bodyHandler(body -> {
			int statusCode = response.statusCode();
			System.out.println("APIGateway.getQA() statusCode: "+statusCode);
			System.out.println("APIGateway.getQA() body: "+body);
//			System.out.println("APIGateway.getQA() RESPONSE: "+body.toJsonArray());
//			handler.handle(Future.succeededFuture(body.toJsonArray()));
			if(statusCode == 404 || statusCode == 500) {
				handler.handle(Future.succeededFuture(body.toString()));
			} else if(statusCode == 200) {
				handler.handle(Future.succeededFuture(body.toJsonArray()));
			}
		})).exceptionHandler(t -> {
			System.out.println("APIGateway.getQA() EXCEPTION");
			t.printStackTrace();
			handler.handle(Future.failedFuture(t));
		}).setTimeout(1000).end();
	}
	
	public void getQA(String uri,Handler<AsyncResult<Object>> handler) {
		System.out.println("APIGateway.getQA() uri>>>>>>>>>>>>>"+uri);
		client.get(uri, response -> response.bodyHandler(body -> {
			int statusCode = response.statusCode();
			System.out.println("APIGateway.getQA() statusCode: "+statusCode);
			System.out.println("APIGateway.getQA() body: "+body);
//			System.out.println("APIGateway.getQA() RESPONSE: "+body.toJsonArray());
//			handler.handle(Future.succeededFuture(body.toJsonArray()));
			if(statusCode == 404 || statusCode == 500) {
				handler.handle(Future.succeededFuture(body.toString()));
			} else if(statusCode == 200) {
				handler.handle(Future.succeededFuture(body.toJsonArray()));
			}
		})).exceptionHandler(t -> {
			System.out.println("APIGateway.getQA() EXCEPTION");
			t.printStackTrace();
			handler.handle(Future.failedFuture(t));
		}).setTimeout(1000).end();
	}

}
