package com.stackOverFlow;

import com.stackOverFlow.common.RestAPIVerticle;

import io.vertx.core.Future;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CookieHandler;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.StaticHandler;

public abstract class DefaultAPIGateway extends RestAPIVerticle {

	protected static final int DEFAULT_PORT = 8787;
	
	private HttpClient client;
	
	@Override
	public void start(Future<Void> future) throws Exception {
		super.start();

		Router router = Router.router(vertx);
		// body handler
		router.route().handler(BodyHandler.create());
		router.route().handler(CookieHandler.create());
		createSessionHandler(router);
		
		router.route().handler(CorsHandler.create("*")
			      .allowedMethod(HttpMethod.GET)
			      .allowedMethod(HttpMethod.POST)
			      .allowedMethod(HttpMethod.PUT)
			      .allowedMethod(HttpMethod.DELETE)
			      .allowedMethod(HttpMethod.OPTIONS)
			      .allowCredentials(true)
			      .allowedHeader("X-PINGARUNER")
			      .allowedHeader("Access-Control-Allow-Method")
			      .allowedHeader("Access-Control-Allow-Origin")
			      .allowedHeader("Access-Control-Allow-Credentials")
			      .allowedHeader("Authorization")
			      .allowedHeader("Content-Type"));

		router.get("/login/").handler(rctx -> {
			System.out.println("APIGateway_2.start() loginnnnnnnnnn");
			System.out.println("APIGateway_2.start() user.dir: " + System.getProperties().getProperty("user.dir"));
			rctx.response().setStatusCode(200).putHeader("Content-Type", "text/html")
					.sendFile("webroot/login/login.html");
		});

		router.get("/questions/").handler(rctx -> {
			System.out.println("APIGateway_2.start() Questionssss");
			rctx.response().setStatusCode(200).putHeader("Content-Type", "text/html")
					.sendFile("webroot/questions/questions.html");
		});
		
		router.get("/questions/results/").handler(rctx -> {
			System.out.println("APIGateway_2.start() Questionssss");
			rctx.response().setStatusCode(200).putHeader("Content-Type", "text/html")
					.sendFile("webroot/questions/questions.html");
		});

		router.get("/questions/ask").handler(rctx -> {
			System.out.println("APIGateway_2.start() Questionssss");
			rctx.response().setStatusCode(200).putHeader("Content-Type", "text/html")
					.sendFile("webroot/questions/ask_question.html");
		});

		router.get("/signup/").handler(rctx -> {
			System.out.println("APIGateway_2.start() Questionssss");
			rctx.response().setStatusCode(200).putHeader("Content-Type", "text/html")
					.sendFile("webroot/signup/signup.html");
		});

		router.get("/questions/:questionId").handler(rctx -> {
			System.out.println("APIGateway_2.start() questions/:questionId: " + rctx.request().getParam("questionId"));
			// JsonObject jsonObj = new JsonObject().put("questionId",
			// rctx.request().getParam("questionId"));

			rctx.response().setStatusCode(200).putHeader("Content-Type", "text/html")
					.sendFile("webroot/questions/question_detail.html");
		});
		
		router.get("/users/profile/").handler(rctx -> {
			System.out.println("APIGateway_2.start() profile");
			rctx.response().setStatusCode(200).putHeader("Content-Type", "text/html")
					.sendFile("webroot/profile/profile.html");
		});
		
		router.get("/users/profile/:userId").handler(rctx -> {
			System.out.println("APIGateway_2.start() USER profile");
			rctx.response().setStatusCode(200).putHeader("Content-Type", "text/html")
					.sendFile("webroot/profile/profile.html");
		});
		
		router.get("/users/profile/edit/").handler(rctx -> {
			System.out.println("APIGateway_2.start() EDIT profile");
			rctx.response().setStatusCode(200).putHeader("Content-Type", "text/html")
					.sendFile("webroot/profile/edit_profile.html");
		});
		
		router.get("/chat/").handler(rctx -> {
			System.out.println("APIGateway_2.start() EDIT profile");
			rctx.response().setStatusCode(200).putHeader("Content-Type", "text/html")
					.sendFile("webroot/chat/chat.html");
		});
		
		routeRequests(router);
		
		// static content
	    router.route("/*").handler(StaticHandler.create());
	    
	    createHttpServer(router,future);

	}
	
	protected abstract void routeRequests(final Router router);
	
	protected abstract void createSessionHandler(final Router router);
	
	protected abstract void createHttpServer(final Router router,Future<Void> future);
	
	public void close() {
		client.close();
	}

}
