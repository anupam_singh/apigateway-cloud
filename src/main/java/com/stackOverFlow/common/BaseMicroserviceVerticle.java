package com.stackOverFlow.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import io.vertx.circuitbreaker.CircuitBreaker;
import io.vertx.circuitbreaker.CircuitBreakerOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.impl.ConcurrentHashSet;
import io.vertx.core.json.JsonObject;
import io.vertx.servicediscovery.Record;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.ServiceDiscoveryOptions;
import io.vertx.servicediscovery.types.HttpEndpoint;

public abstract class BaseMicroserviceVerticle extends AbstractVerticle {

	protected ServiceDiscovery discovery;
	protected CircuitBreaker circuitBreaker;
	protected Set<Record> registeredRecords = new ConcurrentHashSet<Record>();

	@Override
	public void start() throws Exception {
		// init service discovery instance
		discovery = ServiceDiscovery.create(vertx, new ServiceDiscoveryOptions().setBackendConfiguration(config()));
		
		// init circuit breaker instance
	    JsonObject cbOptions = config().getJsonObject("circuit-breaker") != null ?
	      config().getJsonObject("circuit-breaker") : new JsonObject();
	    circuitBreaker = CircuitBreaker.create(cbOptions.getString("name", "circuit-breaker"), vertx,
	      new CircuitBreakerOptions()
	        .setMaxFailures(cbOptions.getInteger("max-failures", 5))
	        .setTimeout(cbOptions.getLong("timeout", 10000L))
	        .setFallbackOnFailure(true)
	        .setResetTimeout(cbOptions.getLong("reset-timeout", 30000L))
	    );
	}
	
	protected Future<Void> publishHttpEndpoint(String name, String host, int port) {
		System.out.println("BaseMicroserviceVerticle.publishHttpEndpoint() api.name: "+config().getString("api.name", ""));
	    Record record = HttpEndpoint.createRecord(name, host, port, "/",
	      new JsonObject().put("api.name", config().getString("api.name", ""))
	    );
	    return publish(record);
	  }

	  protected Future<Void> publishApiGateway(String host, int port) {
	    Record record = HttpEndpoint.createRecord("api-gateway", true, host, port, "/", null)
	      .setType("api-gateway");
	    return publish(record);
	  }

	  /**
	   * Publish a service with record.
	   *
	   * @param record service record
	   * @return async result
	   */
	  private Future<Void> publish(Record record) {
	    if (discovery == null) {
	      try {
	        start();
	      } catch (Exception e) {
	        throw new IllegalStateException("Cannot create discovery service");
	      }
	    }

	    Future<Void> future = Future.future();
	    // publish the service
	    discovery.publish(record, ar -> {
	      System.out.println("BaseMicroserviceVerticle.publish() ar.succeeded(): "+ar.succeeded());
	      if (ar.succeeded()) {
	        registeredRecords.add(record);
	        System.out.println("Service <" + ar.result().getName() + "> published");
	        future.complete();
	      } else {
	        future.fail(ar.cause());
	      }
	    });

	    return future;
	  }
	  
	  @Override
	  public void stop(Future<Void> future) throws Exception {
	    // In current design, the publisher is responsible for removing the service
	    List<Future> futures = new ArrayList<>();
	    registeredRecords.forEach(record -> {
	      Future<Void> cleanupFuture = Future.future();
	      futures.add(cleanupFuture);
	      discovery.unpublish(record.getRegistration(), cleanupFuture.completer());
	    });

	    if (futures.isEmpty()) {
	      discovery.close();
	      future.complete();
	    } else {
	      CompositeFuture.all(futures)
	        .setHandler(ar -> {
	          discovery.close();
	          if (ar.failed()) {
	            future.fail(ar.cause());
	          } else {
	            future.complete();
	          }
	        });
	    }
	  }

}
