package com.stackOverFlow;

import java.util.List;
import java.util.Optional;

import com.stackOverFlow.common.RestAPIVerticle;

import io.vertx.core.Future;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CookieHandler;
import io.vertx.ext.web.handler.SessionHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.sstore.LocalSessionStore;
import io.vertx.servicediscovery.Record;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.types.HttpEndpoint;

public class APIGateway_2 extends RestAPIVerticle {
	
	private static final int DEFAULT_PORT = 8787;

	private HttpClient client;

	@Override
	public void start(Future<Void> future) throws Exception {
		super.start();
		Router router = Router.router(vertx);
		String host = config().getString("api.gateway.http.address", "localhost");
		int port = config().getInteger("api.gateway.http.port", DEFAULT_PORT);
		System.out.println("APIGateway_2.start() host: "+host);
		// body handler
		router.route().handler(BodyHandler.create());
		router.route().handler(CookieHandler.create());
//		router.route().handler(SessionHandler.create(ClusteredSessionStore.create(vertx)));
		router.route().handler(SessionHandler.create(LocalSessionStore.create(vertx)));
		
		router.get("/login/").handler(rctx -> {
			System.out.println("APIGateway_2.start() loginnnnnnnnnn");
			System.out.println("APIGateway_2.start() user.dir: "+System.getProperties().getProperty("user.dir"));
			rctx.response().setStatusCode(200).putHeader("Content-Type", "text/html")
					.sendFile("webroot/login/login.html");
		});
		
		router.get("/questions/").handler(rctx -> {
			System.out.println("APIGateway_2.start() Questionssss");
			rctx.response().setStatusCode(200).putHeader("Content-Type", "text/html")
					.sendFile("webroot/questions/questions.html");
		});
		
		router.get("/questions/ask").handler(rctx -> {
			System.out.println("APIGateway_2.start() Questionssss");
			rctx.response().setStatusCode(200).putHeader("Content-Type", "text/html")
					.sendFile("webroot/questions/ask_question.html");
		});
		
		router.get("/signup/").handler(rctx -> {
			System.out.println("APIGateway_2.start() Questionssss");
			rctx.response().setStatusCode(200).putHeader("Content-Type", "text/html")
					.sendFile("webroot/signup/signup.html");
		});
		
		router.get("/questions/:questionId").handler(rctx -> {
    	System.out.println("APIGateway_2.start() questions/:questionId: "+rctx.request().getParam("questionId"));
//    	JsonObject jsonObj = new JsonObject().put("questionId", rctx.request().getParam("questionId"));
    	
    	rctx.response().setStatusCode(200).putHeader("Content-Type", "text/html")
		.sendFile("webroot/questions/question_detail.html");

		});
		
		// api dispatcher
//		router.route("/api/*").handler(this::dispatchRequests);
//		router.route("/api/*").handler(this::dispatchRequests_2);
		
		router.route("/api/users/*").handler(rctx -> {
			dispatchRequests_2(rctx,"104.154.248.9");
		});
		
		router.route("/api/questions/*").handler(rctx -> {
			dispatchRequests_2(rctx,"35.190.150.30");
		});
		
		// static content
	    router.route("/*").handler(StaticHandler.create());
//	    router.route("/login/").handler(StaticHandler.create().setWebRoot("/login/"));
	    
		System.out.println("APIGateway_2.start() >>>>>>>>>>>>>>>>>");
//		vertx.createHttpServer().requestHandler(router::accept).listen(port, host, res -> {
//			System.out.println("APIGatewayVerticle.start() res.succeeded(): " + res.succeeded());
//			if (res.succeeded()) {
//				publishApiGateway(host, port);
//				future.complete();
//				System.out.println("API Gateway ANUPMMMMMMMMMMMMMM is running on port " + port);
//			} else {
//				future.fail(res.cause());
//			}
//		});
		vertx.createHttpServer().requestHandler(router::accept).listen(DEFAULT_PORT);
	}
	
	private void dispatchRequests(RoutingContext context) {
		int initialOffset = 5; // length of `/api/`
		// run with circuit breaker in order to deal with failure
		circuitBreaker.execute(future -> {
			getAllEndpoints().setHandler(ar -> {
				if (ar.succeeded()) {
					List<Record> recordList = ar.result();
					System.out.println("APIGatewayVerticle.dispatchRequests() recordList size: "+recordList.size());
					if(recordList.size() > 0) {
						System.out.println("APIGatewayVerticle.dispatchRequests() REST record: "+recordList.get(0).toJson());
					}
					// get relative path and retrieve prefix to dispatch client
					String path = context.request().uri();
					System.out.println("APIGatewayVerticle.dispatchRequests() path: "+path);
					if (path.length() <= initialOffset) {
						notFound(context);
						future.complete();
						return;
					}
					System.out.println("APIGateway_2.dispatchRequests() path.substring(initialOffset): "+path.substring(initialOffset));
					String prefix = (path.substring(initialOffset).split("/"))[0];
					System.out.println("APIGateway_2.dispatchRequests() prefix: "+prefix);
					// generate new relative path
//					String newPath = path.substring(initialOffset + prefix.length());
					String newPath = "/" + path.substring(initialOffset);
					System.out.println("APIGatewayVerticle.dispatchRequests() newPath: "+newPath);
					// get one relevant HTTP client, may not exist
					Optional<Record> client = recordList.stream()
							.filter(record -> record.getMetadata().getString("api.name") != null)
							.filter(record -> record.getMetadata().getString("api.name").equals(prefix)).findAny(); // simple
																													// load
																													// balance
					System.out.println("APIGatewayVerticle.dispatchRequests() client.isPresent(): "+client.isPresent());
					if (client.isPresent()) {
						System.out.println("APIGatewayVerticle.dispatchRequests() client.get(): "+client.get().toJson());
						System.out.println("APIGatewayVerticle.dispatchRequests() getReference client.get(): "+discovery.getReference(client.get()).record().toJson());
						System.out.println("APIGatewayVerticle.dispatchRequests() newPath: "+newPath);
						doDispatch(context, newPath, discovery.getReference(client.get()).get(), future);
					} else {
						notFound(context);
						future.complete();
					}
				} else {
					future.fail(ar.cause());
				}
			});
		}).setHandler(ar -> {
			if (ar.failed()) {
				badGateway(ar.cause(), context);
			}
		});
	}
	
	private void dispatchRequests_2(RoutingContext context,String host) {
		int initialOffset = 5; // length of `/api/`
		// run with circuit breaker in order to deal with failure
		circuitBreaker.execute(future -> {

			// get relative path and retrieve prefix to dispatch client
			String path = context.request().uri();
			System.out.println("APIGatewayVerticle.dispatchRequests() path: "+path);
			if (path.length() <= initialOffset) {
				notFound(context);
				future.complete();
				return;
			}
			System.out.println("APIGateway_2.dispatchRequests() path.substring(initialOffset): "+path.substring(initialOffset));
			String prefix = (path.substring(initialOffset).split("/"))[0];
			System.out.println("APIGateway_2.dispatchRequests() prefix: "+prefix);
			// generate new relative path
//			String newPath = path.substring(initialOffset + prefix.length());
			String newPath = "/" + path.substring(initialOffset);
			System.out.println("APIGatewayVerticle.dispatchRequests() newPath: "+newPath);
			doDispatch_2(context, newPath, host,future);
		
		}).setHandler(ar -> {
			if (ar.failed()) {
				badGateway(ar.cause(), context);
			}
		});
	}
	
	private void doDispatch_2(RoutingContext context, String path, String host, Future<Object> cbFuture) {
		HttpClient client = vertx.createHttpClient();

		System.out.println(
				"APIGateway_2.doDispatch() path: " + path + " context.request().uri(): " + context.request().uri());
		System.out.println("APIGateway_2.doDispatch() Authorization header: "+context.request().getHeader("Authorization"));
		System.out.println("APIGateway_2.doDispatch() host: "+host);
		HttpClientRequest toReq = client.request(context.request().method(), 80, host, path, response -> {
			response.bodyHandler(body -> {
				System.out.println("APIGateway_2.doDispatch() response: "+response);
				System.out.println("APIGateway_2.doDispatch() response.statusCode(): " + response.statusCode());
				if (response.statusCode() >= 500) { // api endpoint server
													// error, circuit breaker
													// should fail
					cbFuture.fail(response.statusCode() + ": " + body.toString());
				} else {
					HttpServerResponse toRsp = context.response().setStatusCode(response.statusCode());
					response.headers().forEach(header -> {
						System.out.println("APIGateway_2.doDispatch() header.getKey(): "+header.getKey());
						System.out.println("APIGateway_2.doDispatch() header.getValue(): "+header.getValue());
						toRsp.putHeader(header.getKey(), header.getValue());
					});
					System.out.println("APIGateway_2.doDispatch() body: "+body);
					System.out.println("APIGateway_2.doDispatch() body.toString(): "+body.toString());
					// send response
					toRsp.end(body);
					cbFuture.complete();
				}
			});
		});
		// set headers
		context.request().headers().forEach(header -> {
			toReq.putHeader(header.getKey(), header.getValue());
		});
		if (context.user() != null) {
			toReq.putHeader("user-principal", context.user().principal().encode());
		}
		System.out.println("APIGateway_2.doDispatch() context.getBody(): "+context.getBody());
		// send request
		if (context.getBody() == null) {
			toReq.end();
		} else {
			toReq.end(context.getBody());
		}
	
	}
	
	/**
	   * Dispatch the request to the downstream REST layers.
	   *
	   * @param context routing context instance
	   * @param path    relative path
	   * @param client  relevant HTTP client
	   */
	private void doDispatch(RoutingContext context, String path, HttpClient client, Future<Object> cbFuture) {
		System.out.println(
				"APIGateway_2.doDispatch() path: " + path + " context.request().uri(): " + context.request().uri());
		System.out.println("APIGateway_2.doDispatch() Authorization header: "+context.request().getHeader("Authorization"));
		HttpClientRequest toReq = client.request(context.request().method(), path, response -> {
			response.bodyHandler(body -> {
				System.out.println("APIGateway_2.doDispatch() response.statusCode(): " + response.statusCode());
				if (response.statusCode() >= 500) { // api endpoint server
													// error, circuit breaker
													// should fail
					cbFuture.fail(response.statusCode() + ": " + body.toString());
				} else {
					HttpServerResponse toRsp = context.response().setStatusCode(response.statusCode());
					response.headers().forEach(header -> {
						System.out.println("APIGateway_2.doDispatch() header.getKey(): "+header.getKey());
						System.out.println("APIGateway_2.doDispatch() header.getValue(): "+header.getValue());
						toRsp.putHeader(header.getKey(), header.getValue());
					});
					System.out.println("APIGateway_2.doDispatch() body: "+body);
					System.out.println("APIGateway_2.doDispatch() body.toString(): "+body.toString());
					// send response
					toRsp.end(body);
					cbFuture.complete();
				}
				ServiceDiscovery.releaseServiceObject(discovery, client);
			});
		});
		// set headers
		context.request().headers().forEach(header -> {
			toReq.putHeader(header.getKey(), header.getValue());
		});
		if (context.user() != null) {
			toReq.putHeader("user-principal", context.user().principal().encode());
		}
		System.out.println("APIGateway_2.doDispatch() context.getBody(): "+context.getBody());
		// send request
		if (context.getBody() == null) {
			toReq.end();
		} else {
			toReq.end(context.getBody());
		}
	}
	
	/**
	 * Get all REST endpoints from the service discovery infrastructure.
	 *
	 * @return async result
	 */
	private Future<List<Record>> getAllEndpoints() {
		Future<List<Record>> future = Future.future();
		discovery.getRecords(record -> record.getType().equals(HttpEndpoint.TYPE), future.completer());
		return future;
	}

	public void close() {
		client.close();
	}
	
}
