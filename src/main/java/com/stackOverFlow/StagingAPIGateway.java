package com.stackOverFlow;

import java.util.List;
import java.util.Optional;

import io.vertx.core.Future;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.SessionHandler;
import io.vertx.ext.web.sstore.ClusteredSessionStore;
import io.vertx.servicediscovery.Record;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.types.HttpEndpoint;

public class StagingAPIGateway extends DefaultAPIGateway{

	@Override
	protected void routeRequests(final Router router) {
		// api dispatcher
		router.route("/api/*").handler(this::dispatchRequests);
	}
	
	private void dispatchRequests(RoutingContext context) {
		// run with circuit breaker in order to deal with failure
		circuitBreaker.execute(future -> {
			getAllEndpoints().setHandler(ar -> {
				if (ar.succeeded()) {
					int initialOffset = 5; // length of `/api/`
					List<Record> recordList = ar.result();
					System.out.println("StagingAPIGateway.dispatchRequests() recordList size: "+recordList.size());
					if(recordList.size() > 0) {
						System.out.println("StagingAPIGateway.dispatchRequests() REST record: "+recordList.get(0).toJson());
					}
					// get relative path and retrieve prefix to dispatch client
					String path = context.request().uri();
					System.out.println("StagingAPIGateway.dispatchRequests() path: "+path);
					if (path.length() <= initialOffset) {
						notFound(context);
						future.complete();
						return;
					}
					String subStr = path.substring(initialOffset);
					System.out.println("StagingAPIGateway.dispatchRequests() path.substring(initialOffset): "+subStr);
					String[] strArray = subStr.split("/");
					System.out.println("StagingAPIGateway.dispatchRequests() strArray[0]: "+strArray[0]);
					String prefix = strArray[0] + "." + strArray[1];
					System.out.println("StagingAPIGateway.dispatchRequests() prefix: "+prefix);
					// generate new relative path
//					String newPath = path.substring(initialOffset + prefix.length());
//					String newPath = "/";
					System.out.println("StagingAPIGateway.dispatchRequests() strArray.length: "+strArray.length);
//					for (int i = 1; i < strArray.length; i++) {
//						System.out.println("StagingAPIGateway.dispatchRequests() strArray[i]: "+strArray[i]);
//						newPath = newPath.concat(strArray[i]).concat("/");
//					}
					initialOffset = initialOffset + strArray[0].length() + 1;
					System.out.println("StagingAPIGateway.dispatchRequests() initialOffset: "+initialOffset);
					String newPath = "/" + path.substring(initialOffset);
					System.out.println("StagingAPIGateway.dispatchRequests() newPath: "+newPath);
					// get one relevant HTTP client, may not exist
					Optional<Record> client = recordList.stream()
							.filter(record -> record.getMetadata().getString("api.name") != null)
							.filter(record -> record.getMetadata().getString("api.name").equals(prefix)).findAny(); // simple
																													// load
																													// balance
					System.out.println("StagingAPIGateway.dispatchRequests() client.isPresent(): "+client.isPresent());
					if (client.isPresent()) {
						System.out.println("StagingAPIGateway.dispatchRequests() client.get(): "+client.get().toJson());
						System.out.println("StagingAPIGateway.dispatchRequests() getReference client.get(): "+discovery.getReference(client.get()).record().toJson());
						System.out.println("StagingAPIGateway.dispatchRequests() newPath: "+newPath);
						doDispatch(context, newPath, discovery.getReference(client.get()).get(), future);
					} else {
						notFound(context);
						future.complete();
					}
				} else {
					future.fail(ar.cause());
				}
			});
		}).setHandler(ar -> {
			if (ar.failed()) {
				badGateway(ar.cause(), context);
			}
		});
	}
	
	/**
	   * Dispatch the request to the downstream REST layers.
	   *
	   * @param context routing context instance
	   * @param path    relative path
	   * @param client  relevant HTTP client
	   */
	private void doDispatch(RoutingContext context, String path, HttpClient client, Future<Object> cbFuture) {
		System.out.println(
				"StagingAPIGateway.doDispatch() path: " + path + " context.request().uri(): " + context.request().uri());
		System.out.println("StagingAPIGateway.doDispatch() Authorization header: "+context.request().getHeader("Authorization"));
		HttpClientRequest toReq = client.request(context.request().method(), path, response -> {
			response.bodyHandler(body -> {
				System.out.println("StagingAPIGateway.doDispatch() response.statusCode(): " + response.statusCode());
				if (response.statusCode() >= 500) { // api endpoint server
													// error, circuit breaker
													// should fail
					cbFuture.fail(response.statusCode() + ": " + body.toString());
				} else {
					HttpServerResponse toRsp = context.response().setStatusCode(response.statusCode());
					response.headers().forEach(header -> {
						System.out.println("StagingAPIGateway.doDispatch() header.getKey(): "+header.getKey());
						System.out.println("StagingAPIGateway.doDispatch() header.getValue(): "+header.getValue());
						toRsp.putHeader(header.getKey(), header.getValue());
					});
//					System.out.println("StagingAPIGateway.doDispatch() body: "+body);
//					System.out.println("StagingAPIGateway.doDispatch() body.toString(): "+body.toString());
					// send response
					toRsp.end(body);
					cbFuture.complete();
				}
				ServiceDiscovery.releaseServiceObject(discovery, client);
			});
		});
		// set headers
		context.request().headers().forEach(header -> {
			toReq.putHeader(header.getKey(), header.getValue());
		});
		if (context.user() != null) {
			toReq.putHeader("user-principal", context.user().principal().encode());
		}
		System.out.println("StagingAPIGateway.doDispatch() context.getBody(): "+context.getBody());
		// send request
		if (context.getBody() == null) {
			toReq.end();
		} else {
			toReq.end(context.getBody());
		}
	}
	
	/**
	 * Get all REST endpoints from the service discovery infrastructure.
	 *
	 * @return async result
	 */
	private Future<List<Record>> getAllEndpoints() {
		Future<List<Record>> future = Future.future();
		discovery.getRecords(record -> record.getType().equals(HttpEndpoint.TYPE), future.completer());
		return future;
	}

	@Override
	protected void createHttpServer(final Router router,Future<Void> future) {
		String host = config().getString("api.gateway.http.address", "localhost");
		int port = config().getInteger("api.gateway.http.port", DEFAULT_PORT);
		System.out.println("StagingAPIGateway.createHttpServer() host: "+host);
		vertx.createHttpServer().requestHandler(router::accept).listen(port, host, res -> {
			System.out.println("StagingAPIGateway.createHttpServer() res.succeeded(): " + res.succeeded());
			if (res.succeeded()) {
				publishApiGateway(host, port);
				future.complete();
				System.out.println("StagingAPIGateway ANUPMMMMMMMMMMMMMM is running on port " + port);
			} else {
				future.fail(res.cause());
			}
		});
	}

	@Override
	protected void createSessionHandler(Router router) {
		router.route().handler(SessionHandler.create(ClusteredSessionStore.create(vertx)));
	}

}
