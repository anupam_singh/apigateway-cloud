addEventListener("load", function() {
	console.log("COMMON LOADED");
	console.log(" COMMON window.localStorage.accessToken: "+window.localStorage.accessToken);
	var token = window.localStorage.accessToken;
	if (typeof(token) !== 'undefined') {
		
		var xhr = new XMLHttpRequest();
		xhr.open("get", "/api/v1/users/", true);
		xhr.setRequestHeader("Content-Type", "text/html");
		xhr.setRequestHeader("Authorization", token);
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				if(xhr.status == 200) {
					console.log(" users responseText: "+xhr.responseText);
					var userObj = JSON.parse(xhr.responseText);
					document.getElementById("user_info_id").innerHTML = "Hi " + userObj._id;
					document.getElementById("user_info_id").style.display = "block";
					document.getElementById("logout_id").style.display = "block";
					document.getElementById("logout_id").addEventListener("click", function(e) {
						console.log("LOG OUT clicked");
						window.localStorage.clear();
						logout();
					});
					document.getElementById("user_info_id").addEventListener("click", function(e) {
						console.log("USER INFO clicked");
						window.location.href = "/users/profile/";
					});
				} else if(xhr.status == 401 || xhr.status == 404) {
					window.localStorage.clear();
					window.location.href = "/";
				}
			} 
		};
		xhr.send();
		
	} else {
		document.getElementById("login_id").style.display = "block";
		document.getElementById("signup_id").style.display = "block";
		document.getElementById("login_id").addEventListener("click", function(e) {
			console.log("LOG IN clicked");
			window.location.href = "/login/"
		});
		document.getElementById("signup_id").addEventListener("click", function(e) {
			console.log("SIGN UP clicked");
			window.location.href = "/signup/"
		});
	}
	
	document.getElementById("all_question_id").addEventListener("click", function(e) {
		console.log("ALL QUESTIONS clicked");
		window.localStorage.targetpage = "ALL_QUESTIONS";
		window.location.href = "/questions/";
	});
	
	document.getElementById("ask_question_id").addEventListener("click", function(e) {
		console.log("ASK QUESTIONS clicked");
		window.location.href = "/questions/ask";
	});
	
	document.getElementById("home_id").addEventListener("click", function(e) {
		console.log("HOME clicked");
		window.location.href = "/";
	});
	
	document.getElementById("stack_overflow_id").addEventListener("click", function(e) {
		console.log("STACK OVER FLOW clicked");
		window.location.href = "/";
	});
	
	document.getElementById("search_submit_id").addEventListener("click", function(e) {
		console.log("SEARCH SUBMIT clicked");
		window.localStorage.targetpage = "SEARCHED_QUESTIONS";
		var query_str = document.getElementById("search_input_id").value;
		window.location.href = "/questions/results/?search_query=" + query_str;
	});
	
	function logout() {
		var xhr = new XMLHttpRequest();
		xhr.open("get", "/api/v1/users/", true);
		xhr.setRequestHeader("Content-Type", "text/html");
		xhr.setRequestHeader("Authorization", window.localStorage.accessToken);
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 401)) {
				window.location.href = "/";
			} 
		};
		xhr.send();
	}
	
});