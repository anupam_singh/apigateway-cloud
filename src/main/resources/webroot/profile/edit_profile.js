addEventListener("load", function() {
	console.log("EDIT PROFILE LOADED");
//	console.log(" EDIT PROFILE window.localStorage.accessToken: "+window.localStorage.accessToken);
	var token = window.localStorage.accessToken;
	var userId = window.localStorage.userId;
	var userObj = JSON.parse(window.localStorage.getItem('userObj'));
//	console.log("EDIT PROFILE userObj._id: "+userObj._id);
//	console.log("EDIT PROFILE userId: "+userId);
//	console.log("EDIT PROFILE userObj.name: "+userObj.name);
	if(typeof(token) !== 'undefined' && userId == userObj._id) {
		document.getElementById("edit_name_id").placeholder = userObj.name;
		document.getElementById("edit_city_name_id").placeholder = userObj.city;
		document.getElementById("update_profile_button_id").addEventListener("click", function(e) {
			console.log("UPDATE PROFILE clicked");
			var update_details_json = {};
			var new_name = document.getElementById("edit_name_id").value;
			if(new_name != "" && new_name != "undefined") {
				update_details_json ["name"] = new_name;
			}
			var new_city = document.getElementById("edit_city_name_id").value;
			if(new_city != "" && new_city != "undefined") {
				update_details_json ["city"] = new_city;
			}
			console.log("update_details_json::::::::: "+update_details_json);
			if(JSON.stringify(update_details_json) === '{}') {
				console.log("NO UPDATE");
				document.getElementById("update_field_empty_id").style.display = "block";
				return;
			}
//			if(update_details_json.length == 0) {
//				console.log("NO UPDATE");
//				return;
//			}
			update_details_json ["username"] = userId;
//			var update_details_json = {
//					"username" : userId,
//					"name" : document.getElementById("edit_name_id").value,
//					"city" : document.getElementById("edit_city_name_id").value
//			};
			var xhr = new XMLHttpRequest();
			xhr.open("put", "/api/v2/users/", true);
			xhr.setRequestHeader("Content-Type", "text/html");
			xhr.setRequestHeader("Authorization", token);
			xhr.onreadystatechange = function() {
				if (xhr.readyState == 4) {
					console.log("UPDATE PROFILE xhr.status: "+xhr.status)
					if(xhr.status == 200) {
						window.location.href = "/users/profile/";
					} 
					else if(xhr.status == 401 || xhr.status == 404) {
						window.localStorage.clear();
						window.location.href = "/";
					} else if(xhr.status ==  502) {
						document.getElementById("update_field_empty_id").style.display = "block";
						document.getElementById("update_field_empty_id").innerHTML="UPDATE feature is in Version 2 of User Service";
					}
				} 
			};
			xhr.send(JSON.stringify(update_details_json));
		});
	} else {
		window.localStorage.clear();
		window.location.href = "/";
	}
});