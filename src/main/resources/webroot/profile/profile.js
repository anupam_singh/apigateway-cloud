addEventListener("load", function() {
	console.log("PROFILE LOADED");
	console.log(" PROFILE window.localStorage.accessToken: "+window.localStorage.accessToken);
	var token = window.localStorage.accessToken;
	console.log(" PROFILE window.location.href: "+window.location.href);
	var res = window.location.href.split("/");
	var parameter = res[res.length - 1];
	console.log(" PROFILE parameter: "+parameter);
	var url = "";
	if(parameter != "" && parameter != "undefined") {
		url = "/api/v1/users/profile/"+parameter;
	} else {
		url = "/api/v1/users/profile/"
	}
	if (typeof(token) !== 'undefined' || (parameter != "" && parameter != "undefined")) {
		var xhr = new XMLHttpRequest();
//		xhr.open("get", "/api/users/detail/", true);
		console.log("url: "+url);
		xhr.open("get", url, true);
		xhr.setRequestHeader("Content-Type", "text/html");
		xhr.setRequestHeader("Authorization", token);
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				console.log(" xhr.status: "+xhr.status);
				if(xhr.status == 200) {
					console.log(" users responseText: "+xhr.responseText);
					var userObj = JSON.parse(xhr.responseText);
					document.getElementById("panel_title_id").innerHTML = userObj.name;
					document.getElementById("city_name_id").innerHTML = userObj.city;
					var userId = window.localStorage.userId;
					console.log("userId: "+userId);
					console.log(" userObj.username: "+userObj._id);
//					console.log("userId: "+userId + " userObj.username: "+userObj._id + "  toke type: "+typeof(token) !== 'undefined');
					if(typeof(token) !== 'undefined' && userId == userObj._id) {
						document.getElementById("edit_profile_button_id").style.display = "block";
						document.getElementById("edit_profile_button_id").addEventListener("click", function(e) {
							console.log("EDIT PROFILE clicked");
							window.localStorage.setItem('userObj', xhr.responseText);
							console.log(" window.localStorage.userObj._id: "+JSON.parse(window.localStorage.getItem('userObj'))._id);
							window.location.href = "/users/profile/edit/";
						});
					}
				} 
				else if(xhr.status == 401 || xhr.status == 404) {
					window.localStorage.clear();
					window.location.href = "/";
				}
			} 
		};
		xhr.send();
	}
});