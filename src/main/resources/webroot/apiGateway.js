var wsocket;
//var serviceLocation = "ws://localhost:8030/chat/";
//var serviceLocation = "ws://35.188.30.200:80/chat/";
var serviceLocation = "ws://35.197.28.5:80/chat/";
var selected_id;
var current_loggedin_user;
var last_selected_list_item;
var user_id_map = [];
function connectToChatserver(user_id) {
	wsocket = new WebSocket(serviceLocation + user_id);
	wsocket.onmessage = onMessageReceived;
}

function clearContainer_base_div() {
	var msg_container_base_div = document.getElementById("msg_container_base_id");
	var fc = msg_container_base_div.firstChild;
	while( fc ) {
		msg_container_base_div.removeChild( fc );
	    fc = msg_container_base_div.firstChild;
	}
}

function createMessageElement(msgJson) {
	var msg_container_base_div = document.getElementById("msg_container_base_id");
	var container_base_sent_div = document.createElement("div");
	if(current_loggedin_user == msgJson.sender_id) {
		container_base_sent_div.className = "row msg_container base_sent";
	} else {
		container_base_sent_div.className = "row msg_container base_receive";
	}
	msg_container_base_div.appendChild(container_base_sent_div);
	
	var div_1 = document.createElement("div");
	div_1.className = "col-md-10 col-xs-10";
	container_base_sent_div.appendChild(div_1);
	
	var div_2 = document.createElement("div");
	if(current_loggedin_user == msgJson.sender_id) {
		div_2.className = "messages msg_sent";
		div_2.style.backgroundColor = "hsla(120,100%,75%,0.3)";
	} else {
		div_2.className = "messages msg_receive";
		div_2.style.backgroundColor = "rgba(255,255,0,0.3)";
	}
	div_1.appendChild(div_2);
	
	var paragraph = document.createElement("p");
	paragraph.innerHTML = msgJson.message;
	div_2.appendChild(paragraph);
	var time = document.createElement("time");
	time.innerHTML = msgJson.received_time;
	time.style.color = "rgb(128,128,128)";
	div_2.appendChild(time);
}

function onMessageReceived(evt) {
	var data = evt.data;
	console.log("received evt.data: "+data);
	var msg = JSON.parse(evt.data); // native API
//	var $messageLine = $('<tr><td class="received">' + msg.received
//			+ '</td><td class="user label label-info">' + msg.sender
//			+ '</td><td class="message badge">' + msg.message
//			+ '</td></tr>');
//	$chatWindow.append($messageLine);

	var sender_id = msg.sender_id;
	var reciever_id = msg.reciever_id;
	console.log("received sender_id: "+sender_id);
	var chosenId = selected_id;
	if(selected_id != reciever_id && selected_id != sender_id) {
		if(current_loggedin_user != reciever_id) {
			chosenId = reciever_id;
		} else if(current_loggedin_user != sender_id) {
			chosenId = sender_id;
		}
		console.log("document.getElementById(chosenId): "+document.getElementById(chosenId));
		document.getElementById(chosenId).style.backgroundColor = "rgb(152,251,152)";
	}
	var item_obj = window.localStorage.getItem(chosenId);
	console.log("inside receive item_obj: "+item_obj);
	var msgJson = {
			"id" : chosenId,
			"sender_id" : sender_id,
			"reciever_id" : reciever_id,
			"message" : msg.message,
			"received_time" : msg.received_time
	};
	if (!item_obj) {
		var chatMsg = [];
		chatMsg[0] = msgJson;
		window.localStorage.setItem(chosenId, JSON.stringify(chatMsg));
	} else {
		var storedObj = JSON.parse(item_obj);
		console.log("inside receive storedObj: "+storedObj + " storedObj.length: "+storedObj.length);
		storedObj[storedObj.length] = msgJson;
		window.localStorage.setItem(chosenId, JSON.stringify(storedObj));
	}
	var item_obj_2 = window.localStorage.getItem(chosenId);
	var storedObj_2 = JSON.parse(item_obj_2);
	console.log("selected_id: "+selected_id);
	console.log("chosenId : "+chosenId);
	console.log("sender_id: "+sender_id);
	console.log("reciever_id: "+reciever_id);
	if(selected_id == reciever_id || selected_id == sender_id) {
		clearContainer_base_div();
		for (var i = 0; i < storedObj_2.length; i++) {
			console.log("storedObj item: "+storedObj_2[i]);
			createMessageElement(storedObj_2[i])
		}
	}
}

addEventListener("load", function() {
	console.log("HOME PAGE LOADED");
	console.log(" HOME PAGE window.localStorage.accessToken: "+window.localStorage.accessToken);
	getNewestQuestions();
	getAllUsers();
	
	function getNewestQuestions() {
		var xhr = new XMLHttpRequest();

		xhr.open("get", "/api/v1/questions/newest/", true);
		xhr.setRequestHeader("Content-Type", "text/html");
//		xhr.setRequestHeader("Authorization", window.localStorage.accessToken);
		xhr.onreadystatechange = function() {
			console.log("HOME PAGE LOADED xhr.readyState: "+xhr.readyState);
			if (xhr.readyState == 4) {
				console.log("HOME PAGE LOADED xhr.status: "+xhr.status);
				var newest_div_id = document.getElementById("newest_div_id");
				if(xhr.status == 200 || xhr.status == 401) {
//					console.log(" getNewestQuestions responseText: "+xhr.responseText);
					var questionObj = JSON.parse(xhr.responseText);
					newest_div_id.style.display = "block";
					if(questionObj.length == 0) {
						newest_div_id.innerHTML = "Question is yet to be asked";
					} else {
						for (var i = 0; i < questionObj.length; i++) {
							console.log(" getNewestQuestions title: "+questionObj[i].title);
							
							//DIV for one question element
							var main_DIV = document.createElement("div");
							newest_div_id.appendChild(main_DIV);
							
							createTitleDiv(questionObj[i],main_DIV);
							
							var myhr = document.createElement('hr');
							main_DIV.appendChild(myhr);
						}
					}
				} else if(xhr.status == 500 || xhr.status == 404) {
					newest_div_id.style.display = "block";
					newest_div_id.innerHTML = "Question is yet to be asked";
				}
			} 
		};
		xhr.send();
	}
	
	function getAllUsers() {
		var token = window.localStorage.accessToken;
		if (typeof(token) !== 'undefined') {
			var xhr = new XMLHttpRequest();
			xhr.open("get", "/api/v1/users/", true);
			xhr.setRequestHeader("Content-Type", "text/html");
			xhr.setRequestHeader("Authorization", token);
			xhr.onreadystatechange = function() {
				if (xhr.readyState == 4) {
					if(xhr.status == 200) {
						var loggedInUserObj = JSON.parse(xhr.responseText);
						var xhr_all_user = new XMLHttpRequest();
						xhr_all_user.open("get", "/api/v2/users/chat/", true);
						xhr_all_user.setRequestHeader("Content-Type", "text/html");
						xhr_all_user.setRequestHeader("Authorization", token);
						xhr_all_user.onreadystatechange = function() {
							if (xhr_all_user.readyState == 4 && xhr_all_user.status == 200) {
								current_loggedin_user = loggedInUserObj._id;
								console.log("loggedInUserObj._id: "+loggedInUserObj._id)
								connectToChatserver(loggedInUserObj._id);
								var chat_container = document.getElementById("chat_id");
								chat_container.style.display = "block";
								console.log(" all users responseText: "+xhr_all_user.responseText);
								var userObj = JSON.parse(xhr_all_user.responseText);
								
								var skipCounter;
								for (var i = 0; i < userObj.length; i++) {
//									var item_obj = window.localStorage.getItem(userObj[i]._id);
//									if (typeof(token) == 'undefined' || item_obj.length == 0) {
////										add empty space
//									}
//									var skipCounter;
//									if(userObj[i]._id == loggedInUserObj._id) {
//										skipCounter = i;
//										continue;
//									}
									user_id_map[userObj[i]._id] = userObj[i];
									console.log("count i: "+i);
									console.log("userObj[i]._id: "+userObj[i]._id);
									var list_group = document.getElementById("chat_list_group_id");
									var list_item = document.createElement("a");
									if(i == 0) {
										var chat_friend_id = document.getElementById("chat_friend_id");
										chat_friend_id.innerHTML = userObj[i].name;
										var chat_friend_active_id = document.getElementById("chat_friend_active_id");
										if(userObj[i].status == "offline") {
											chat_friend_active_id.style.display = "block";
											chat_friend_active_id.innerHTML = userObj[i].active;
										} else {
											chat_friend_active_id.style.display = "None";
										}
										selected_id = userObj[i]._id;
										var first_item_obj = window.localStorage.getItem(userObj[i]._id);
										if(first_item_obj) {
											var first_storedObj = JSON.parse(first_item_obj);
											for (var j = 0; j < first_storedObj.length; j++) {
												createMessageElement(first_storedObj[j]);
											}
										}
										last_selected_list_item = userObj[i]._id;
										list_item.style.backgroundColor = "rgb(190,190,190)";
									} else {
										list_item.style.backgroundColor = "rgb(248,248,248)";
									}
//									list_item.id = userObj[i]._id + ":" + userObj[i].name;
									list_item.id = userObj[i]._id;
									list_item.className = "list-group-item";
//									list_item.href="#"
									list_item.innerHTML = userObj[i].name + "         " + userObj[i].status;
									list_item.style.width = "200px";
//									list_item.style.backgroundColor = "rgb(248,248,248)";
									
//									var list_item_h4 = document.createElement("h4");
//									list_item_h4.className = "list-group-item-heading";
//									list_item_h4.innerHTML = userObj[i].name;
									var list_item_p = document.createElement("p");
									list_item_p.className = "list-group-item-text";
									list_item_p.innerHTML = userObj[i].status;
									if(userObj[i].status == "online") {
										list_item.style.color = "green";
									} else if(userObj[i].status == "offline"){
										list_item.style.color = "red";
									}
//									list_item.appendChild(list_item_h4);
//									list_item.appendChild(list_item_p);
									list_group.appendChild(list_item);
//									list_item.appendChild(list_item_p);
									
									list_item.addEventListener("click", function(e) {
										console.log("CHATE USER ITEM CLICKED e.target.id: "+e.target.id);
										if(last_selected_list_item) {
											document.getElementById(last_selected_list_item).style.backgroundColor = "rgb(248,248,248)";
										}
										last_selected_list_item = e.target.id;
										console.log("document.getElementById(e.target.id): "+document.getElementById(e.target.id));
										document.getElementById(e.target.id).style.backgroundColor = "rgb(190,190,190)";
										var msg_container_base_div = document.getElementById("msg_container_base_id");
										var fc = msg_container_base_div.firstChild;
										while( fc ) {
											msg_container_base_div.removeChild( fc );
										    fc = msg_container_base_div.firstChild;
										}
										var list_item_taget_id = e.target.id;
										var chat_friend_id = document.getElementById("chat_friend_id");
//										var userObjSplit = list_item_taget_id.split(":");
//										chat_friend_id.innerHTML = userObjSplit[1];
										chat_friend_id.innerHTML = user_id_map[list_item_taget_id].name;
										
										var chat_friend_active_id = document.getElementById("chat_friend_active_id");
										if(user_id_map[list_item_taget_id].status == "offline") {
											chat_friend_active_id.style.display = "block";
											chat_friend_active_id.style.color = "green";
											chat_friend_active_id.innerHTML = user_id_map[list_item_taget_id].active;
										} else {
											chat_friend_active_id.style.display = "None";
										}
										
										var item_obj = window.localStorage.getItem(list_item_taget_id);
										selected_id = list_item_taget_id;
										console.log("item_obj: "+item_obj);
//										var msg_container_base_div = document.getElementById("msg_container_base_id");
//										if (typeof(token) == 'undefined' || item_obj == "null") {
										if (!item_obj) {
//											add empty space
//											console.log("EMPTY CHAT");
//											
//											var chatMsg = [];
//											var msgJson = {
//													"id" : userObjSplit[0],
//													"name" : userObjSplit[1],
//													"message" : "Anupam Singh startsss:  "
//											};
//											chatMsg[0] = msgJson;
//											createMessageElement(msgJson);
//											window.localStorage.setItem(userObjSplit[0], JSON.stringify(chatMsg));
										} else {
//											msg_container_base_div.appendChild(item_obj[0]);
											var storedObj = JSON.parse(item_obj);
											console.log("storedObj: "+storedObj);
											for (var i = 0; i < storedObj.length; i++) {
												console.log("storedObj item: "+storedObj[i]);
												createMessageElement(storedObj[i])
											}
										}
										
									});
									
//									list_item.onmouseover = function(e) {
//										document.getElementById(e.target.id).style.backgroundColor = "rgb(190,190,190)";
//									}
//									
//									list_item.onmouseout = function(e) {
//										document.getElementById(e.target.id).style.backgroundColor = "rgb(248,248,248)";
//									}
								}
								
								document.getElementById("logout_id").style.display = "block";
								document.getElementById("logout_id").addEventListener("click", function(e) {
									console.log("LOG OUT clicked");
									wsocket.close();
								});
							} 
						};
						xhr_all_user.send();
					}
				} 
			};
			xhr.send();
		}
	}
	
	var send_button = document.getElementById("btn-chat");
	send_button.addEventListener("click", function(e) {
		var msgJson = {
//				"sender_id" : loggedInUserObj._id,
//				"reciever_id" : userObjSplit[0],
				"reciever_id" : selected_id,
//				"sender_name" : loggedInUserObj.name,
//				"reciever_name" : userObjSplit[1],
				"message" : document.getElementById("btn-input").value
		};
		console.log("sending JSON.stringify(msgJson): "+JSON.stringify(msgJson));
		document.getElementById("btn-input").value = "";
		wsocket.send(JSON.stringify(msgJson));
	});
	
	function createTitleDiv(questionObjElem,main_DIV) {
		//DIV for question title
		var title_div = document.createElement("div");
		title_div.id = questionObjElem._id;
		title_div.innerHTML = questionObjElem.title;
		
		title_div.addEventListener("click", function(e) {
//			console.log("QUESTION CLICKED e.target.id: "+e.target.id);
			window.location.href = "/questions/" + e.target.id;
			window.localStorage.questionId = e.target.id;
		})
		main_DIV.appendChild(title_div);
		
		title_div.onmouseover = function(e) {
//			console.log("title_div onmouseover e.target.id: "+e.target.id);
			if(document.getElementById(e.target.id) != null) {
				document.getElementById(e.target.id).style.color = "blue";
				document.getElementById(e.target.id).style.cursor = "pointer";
			}
		}
		title_div.onmouseout = function(e) {
//		console.log("title_div onmouseout e.target.id: "+e.target.id);
			if(document.getElementById(e.target.id) != null) {
				document.getElementById(e.target.id).style.color = "black";
			}
		}
		
		//DIV for posted time
		var div_time_date = document.createElement("div");
		div_time_date.innerHTML = questionObjElem.asked + "  ";
		div_time_date.style.textAlign = "right";
		main_DIV.appendChild(div_time_date);
		
		createUserDiv(questionObjElem,main_DIV);
		
	}
	
	function createUserDiv(questionObjElem,main_DIV) {
		//DIV for user who posted the question
		var div_user = document.createElement("div");
		div_user.id = questionObjElem.user.username + ":" + questionObjElem._id;
		div_user.innerHTML = questionObjElem.user.name;
		div_user.style.color = "green";
		div_user.style.textAlign = "right";
		main_DIV.appendChild(div_user);
		
		div_user.addEventListener("click", function(e) {
//			console.log("USER CLICKED e.target.id: "+e.target.id);
			var res = e.target.id.split(":");
			window.location.href = "/users/profile/"+res[0];
		})
		
		div_user.onmouseover = function(e) {
//			console.log("div_useronmouseover e.target.id: "+e.target.id);
			if(document.getElementById(e.target.id) != null) {
				document.getElementById(e.target.id).style.color = "blue";
				document.getElementById(e.target.id).style.cursor = "pointer";
			}
		}
		div_user.onmouseout = function(e) {
//		console.log("div_user onmouseout e.target.id: "+e.target.id);
			if(document.getElementById(e.target.id) != null) {
				document.getElementById(e.target.id).style.color = "green";
			}
		}
	}
});