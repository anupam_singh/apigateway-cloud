addEventListener("load", function() {
	console.log("QUESTION DETAIL LOADED");
	
	var xhr = new XMLHttpRequest();
	console.log("QUESTION DETAIL LOADED questionId: "+window.localStorage.questionId);
	xhr.open("get", "/api/v1/questions/"+ window.localStorage.questionId, true);
	xhr.setRequestHeader("Content-Type", "text/html");
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && xhr.status == 200) {
			var questionObj = JSON.parse(xhr.responseText);
			if(questionObj.length > 0) {
				var question_detail_div_id = document.getElementById("question_detail_div_id");
				question_detail_div_id.style.display = "block";
				var questionObjElem = questionObj[0];
				console.log("questionObjElem: "+questionObjElem)
//				H3 for title 
				var h3_title = document.createElement("H3");
				h3_title.innerHTML = questionObjElem.title;
				h3_title.style.color = "blue";
				question_detail_div_id.appendChild(h3_title);
				
				displayDescription(questionObjElem,question_detail_div_id,"/api/v1/questions/"+ window.localStorage.questionId + "/comment/","/api/v1/questions/"+ window.localStorage.questionId + "/edit/",true);
				
				createLineBreak(question_detail_div_id);
				createLineBreak(question_detail_div_id);
				
				displayAnswersCount(questionObjElem,question_detail_div_id);
				
				createGreenHR(question_detail_div_id,"0.5px solid green");
				
				displayAnswers(questionObjElem,question_detail_div_id);
				
				displayPostAnswerTextarea();
			}
		} 
	};
	xhr.send();
	
//	common components to answers and questions like edit,comments etc
	function displayCommonComponents(objElem,parent_div,h4_desc,descResult,commentURL,editURL,isQuestion) {
		console.log(" displayCommonComponents isQuestion: "+isQuestion)
		displayEdit(objElem,parent_div,h4_desc,descResult,editURL,isQuestion);
		
		var commentArray = objElem.comments;
		var token = window.localStorage.accessToken;
		if(typeof(token) != "undefined" && (typeof(commentArray) == "undefined" || commentArray.length == 0)) {
			displayAddCommentLabel(objElem,parent_div,commentURL);
		}
		var pixel_1 = "935px";
		//DIV for posted time
		if(isQuestion) {
			displayDateAndTime(objElem.asked,parent_div,pixel_1);
		} else {
			displayDateAndTime(objElem.answered,parent_div,pixel_1);
		}
		
		createUserDiv(objElem,parent_div,pixel_1);
		
		displayComments(objElem,parent_div,commentURL);
	}
	
	function displayDescription(objElem,parent_div,commentURL,editURL,isQuestion) {
		var description = objElem.description;
		var descArray = description.split("\n");
		var descResult = "";
		for (var i = 0; i < descArray.length; i++) {
			descResult = descResult + descArray[i];
			if(i < descArray.length - 1) {
				descResult = descResult + "<br />";
			}
		}
		
//		H4 for description
		var h4_desc = document.createElement("H4");
		h4_desc.innerHTML = descResult;
		parent_div.appendChild(h4_desc);
		console.log(" displayDescription isQuestion: "+isQuestion)
		displayCommonComponents(objElem,parent_div,h4_desc,descResult,commentURL,editURL,isQuestion);
	}
	
	function displayAnswersCount(questionObjElem,parent_div) {
		if(typeof(questionObjElem.answers) != "undefined") {
			var answer_count = questionObjElem.answers.length;
			console.log("answer_count: "+answer_count)
			if(answer_count > 0) {
				var label_ans_count = document.createElement("label");
//				label_ans_count.id = "edit" + questionObjElem.user.username + ":" + questionObjElem._id;
				var ans_string = "";
				if(answer_count == 1) {
					ans_string = "Answer";
				} else {
					ans_string = "Answers";
				}
				label_ans_count.innerHTML = answer_count + " " + ans_string;
				label_ans_count.style.fontSize = "medium";
				label_ans_count.style.color = "green";
				parent_div.appendChild(label_ans_count);
			}
		}
	}
	
	function displayAnswers(questionObjElem,parent_div) {
		if(typeof(questionObjElem.answers) != "undefined") {
			for (var i = 0; i < questionObjElem.answers.length; i++) {
				displayDescription(questionObjElem.answers[i],parent_div,"/api/v1/questions/"+ window.localStorage.questionId + "/answer/" + questionObjElem.answers[i]._id + "/comment/","/api/v1/questions/"+ window.localStorage.questionId + "/answer/" + questionObjElem.answers[i]._id + "/edit/",false);
				createGreenHR(parent_div,"0.1px solid green");
			}
		}
	}
	
	function createGreenHR(parent_div, border) {
		var myhr = document.createElement('hr');
		myhr.style.border = border;
		parent_div.appendChild(myhr);
	}
	
	function createLineBreak(parent_div) {
		var myLineBreak = document.createElement("br");
		parent_div.appendChild(myLineBreak);
	}
	
//	edit questions or answers
	function displayEdit(objElem,parent_div,h4_desc,descResult,editURL,isQuestion) {
		console.log(" displayEdit  isQuestion: "+isQuestion);
		var token = window.localStorage.accessToken;
		var userId = window.localStorage.userId;
		if(token != "undefined" && userId == objElem.user.username) {
			var label_edit = document.createElement("label");
//			label_edit.id = "edit" + objElem.user.username + ":" + objElem._id;
			label_edit.id = "edit" + ":" + objElem._id;
			label_edit.innerHTML = "Edit";
			label_edit.style.fontSize = "medium";
			label_edit.style.color = "orange";
			question_detail_div_id.appendChild(label_edit);
			onmouseActions(label_edit,"orange");
			label_edit.addEventListener("click", function(e) {
				editAction(label_edit,h4_desc,objElem,descResult,editURL,isQuestion);
			})
		}
	}
	
	function displayComments(questionObjElem,parent_div_id,commentURL) {
		var commentArray = questionObjElem.comments;
//		displaying comment
		if(typeof(commentArray) != "undefined" && commentArray.length > 0) {
			var token = window.localStorage.accessToken;
			var div_comment = document.createElement("div");
			div_comment.style.marginLeft = "80px";
			parent_div_id.appendChild(div_comment);
			
			var label_comment = document.createElement("label");
			var comment_count_str = "";
			if(commentArray.length == 1) {
				comment_count_str = "Comment";
			} else {
				comment_count_str = "Comments";
			}
			label_comment.innerHTML = commentArray.length + " " + comment_count_str;
			label_comment.style.fontSize = "medium";
			label_comment.style.color = "green";
			div_comment.appendChild(label_comment);
			
			console.log(" commentArray.length: "+commentArray.length);
			var pixel_2 = "860px";
			for (var j = 0; j < commentArray.length; j++) {
				var commenthr = document.createElement('hr');
				commenthr.style.border = "0.5px solid blue";
				div_comment.appendChild(commenthr);
				var comment_desc = commentArray[j].description;
				console.log(" comment_desc: "+comment_desc);
				var commentDescArray = comment_desc.split("\n");
				var commentDescResult = "";
				for (var k = 0; k < commentDescArray.length; k++) {
					commentDescResult = commentDescResult + commentDescArray[k];
					if(k < commentDescArray.length - 1) {
						commentDescResult = commentDescResult + "<br />";
					}
				}
//				H4 for comment
				var h4_desc = document.createElement("H4");
				h4_desc.innerHTML = commentDescResult;
				div_comment.appendChild(h4_desc);
				displayDateAndTime(commentArray[j].commented,div_comment,pixel_2);
				createUserDiv(commentArray[j],div_comment,pixel_2);
			}
			console.log(" token for comments: "+token)
			if(typeof(token) != "undefined") {
				displayAddCommentLabel(questionObjElem,parent_div_id,commentURL);
			}
		}
	}
	
	function displayDateAndTime(date_time,parent_div,marginLeft) {
		//DIV for posted time
		var div_time_date = document.createElement("div");
//		console.log("date from DB: "+ date_time);
		div_time_date.innerHTML = date_time + "  ";
		div_time_date.style.marginLeft = marginLeft;
		div_time_date.style.color = "green";
		div_time_date.style.backgroundColor = "azure";
		div_time_date.style.height= "30px";
		div_time_date.style.width= "200px";
		parent_div.appendChild(div_time_date);
	}
	
	function displayPostAnswerTextarea() {
		var token = window.localStorage.accessToken;
		if (typeof(token) !== 'undefined') {
			document.getElementById("ask_ques_div_comp_id").style.display = "block";
			document.getElementById("post_ans_button_id").addEventListener("click", function(e) {
				var answer_desc = document.getElementById("answer_desc_id").value;
				var post_ans_details_json = {
					"description" : answer_desc
				};
				var xhr = new XMLHttpRequest();
				xhr.open("put", "/api/v1/questions/"+window.localStorage.questionId + "/answer/", true);
				xhr.setRequestHeader("Content-Type", "application/json");
				xhr.setRequestHeader("Authorization", window.localStorage.accessToken);
				xhr.onreadystatechange = function() {
					if (xhr.readyState == 4) {
						if(xhr.status == 200) {
							window.location.href = "/questions/" + window.localStorage.questionId;
						} else if(xhr.status == 401 || xhr.status == 502) {
							window.localStorage.clear();
							window.localStorage.redirectURL = "/questions/" + window.localStorage.questionId;
							window.location.href = "/login/";
						}
					} 
				};
				xhr.send(JSON.stringify(post_ans_details_json));
			})
		} 
	}
	
//	comment on questions or answers
	function displayAddCommentLabel(objElem,question_detail_div_id,commentURL) {
//		label for comment
		var label_comment = document.createElement("label");
//		label_comment.id = "comment" + objElem.user.username + ":" + objElem._id;
		label_comment.id = "comment" + ":" + objElem._id;
		label_comment.innerHTML = "add a comment";
		label_comment.style.marginLeft = "80px";
		label_comment.style.color = "orange";
		question_detail_div_id.appendChild(label_comment);
		onmouseActions(label_comment,"orange");
		
		label_comment.addEventListener("click", function(e) {
			commentAction(commentURL);
		})
	}
	
//	user for comment or question or answer
	function createUserDiv(objElem,parent_div_id,marginLeft) {
		//DIV for user who posted the question
		var div_user = document.createElement("div");
		div_user.id = objElem.user.username + ":" + objElem._id;
		div_user.innerHTML = objElem.user.name;
		div_user.style.color = "orange";
		div_user.style.backgroundColor = "azure";
		div_user.style.textAlign = "left";
		div_user.style.marginLeft = marginLeft;
		div_user.style.height= "30px";
		div_user.style.width= "200px";
		parent_div_id.appendChild(div_user);
		
		div_user.addEventListener("click", function(e) {
//			console.log("USER CLICKED e.target.id: "+e.target.id);
			var res = e.target.id.split(":");
			window.location.href = "/users/profile/"+res[0];
		})
		
		onmouseActions(div_user,"orange");
	}
	
	function onmouseActions(component,mouseoutcolor) {
		component.onmouseover = function(e) {
//			console.log("component onmouseover e.target.id: "+e.target.id);
			if(document.getElementById(e.target.id) != null) {
				document.getElementById(e.target.id).style.color = "blue";
				document.getElementById(e.target.id).style.cursor = "pointer";
			}
		}
		component.onmouseout = function(e) {
//		console.log("component onmouseout e.target.id: "+e.target.id);
			if(document.getElementById(e.target.id) != null) {
				document.getElementById(e.target.id).style.color = mouseoutcolor;
			}
		}
	}
	
	function editAction(label_edit,h4_desc,objElem,descResult,editURL,isQuestion) {
		
		var modal_content_id = document.getElementById("modal_content_id");
//		DIV for editable title and description
		var div_edit = document.createElement("div");
		modal_content_id.appendChild(div_edit);
		console.log(" on shown is div_edit child of modal_content_id: "+(div_edit.parentNode == modal_content_id));
		console.log(" isQuestion: "+isQuestion);
		if(isQuestion == true) {
//			label for title
			var label_title = document.createElement("label");
			label_title.innerHTML = "Title:"
			div_edit.appendChild(label_title);
//			input to title
			var input_title = document.createElement("input");
			input_title.id = "question_title_id";
			input_title.size = "160";
			input_title.className = "form-control";
			input_title.type = "text";
			input_title.value = objElem.title;
			div_edit.appendChild(input_title);
		}

		
//		document.write('<br />');
		
//		DIV for form group of textarea
		var formgroup = document.createElement("div");
		formgroup.className = "form-group";
		div_edit.appendChild(formgroup);
//		label for description
		var label_desc = document.createElement("label");
		label_desc.innerHTML = "Description:"
		formgroup.appendChild(label_desc);
//		textarea for title
		var textarea_title = document.createElement("textarea");
		textarea_title.id = "question_desc_id";
		textarea_title.className = "form-control";
		textarea_title.rows = "5";
		textarea_title.innerHTML = descResult;
		formgroup.appendChild(textarea_title);
		
		$("#myModal").on('hide.bs.modal', function () {
			console.log(" after hide is div_edit child of modal_content_id: "+(div_edit.parentNode == modal_content_id));
			if(div_edit.parentNode == modal_content_id) {
				modal_content_id.removeChild(div_edit);
			}
		});
		$("#myModal").on('shown.bs.modal', function () {
			document.getElementById("submitbutton_id").addEventListener("click", function(e) {
				console.log("EDIT SUBMIT clicked");
				var edit_details_json = "";
				if(isQuestion) {
					var title = document.getElementById("question_title_id").value;
					var description = document.getElementById("question_desc_id").value;
					var edit_details_json = {
						"title" : title,
						"description" : description,
					};
				} else {
					var description = document.getElementById("question_desc_id").value;
					var edit_details_json = {
						"description" : description,
					};
				}
				
				var xhr = new XMLHttpRequest();
				console.log(" editURL: "+editURL)
				xhr.open("put", editURL, true);
				xhr.setRequestHeader("Content-Type", "application/json");
				console.log("ASK QUESTION LOADED window.localStorage.accessToken: "+window.localStorage.accessToken);
				xhr.setRequestHeader("Authorization", window.localStorage.accessToken);
				xhr.onreadystatechange = function() {
					console.log("ASK QUESTION LOADED xhr.readyState: "+xhr.readyState);
					if (xhr.readyState == 4) {
						$("#myModal").modal("hide");
						console.log("ASK QUESTION LOADED xhr.status: "+xhr.status);
						if(xhr.status == 200) {
							window.location.href = "/questions/" + window.localStorage.questionId;
						}
						else if(xhr.status == 401 || xhr.status == 502) {
							window.localStorage.clear();
							window.location.href = "/login/";
						}
						
					} 
				};
				xhr.send(JSON.stringify(edit_details_json));
			});
		});
		$("#myModal").modal();
	}
	
	function commentAction(commentURL) {
		var modal_content_id = document.getElementById("modal_content_id");
		var div_comment = document.createElement("div");
		modal_content_id.appendChild(div_comment);
//		DIV for form group of textarea
		var formgroup = document.createElement("div");
		formgroup.className = "form-group";
		div_comment.appendChild(formgroup);
//		label for description
		var label_comment = document.createElement("label");
		label_comment.innerHTML = "Add a comment:"
		formgroup.appendChild(label_comment);
//		textarea for title
		var textarea_comment = document.createElement("textarea");
		textarea_comment.id = "comment_desc_id";
		textarea_comment.className = "form-control";
		textarea_comment.rows = "5";
		formgroup.appendChild(textarea_comment);
		
		$("#myModal").on('hide.bs.modal', function () {
			console.log(" after hide is div_comment child of modal_content_id: "+(div_comment.parentNode == modal_content_id));
			if(div_comment.parentNode == modal_content_id) {
				modal_content_id.removeChild(div_comment);
			}
		});
		$("#myModal").on('shown.bs.modal', function () {
			document.getElementById("submitbutton_id").addEventListener("click", function(e) {
				console.log("COMMENT SUBMIT clicked");
				var comments = document.getElementById("comment_desc_id").value;
				var edit_details_json = {
					"description" : comments,
				};
				var xhr = new XMLHttpRequest();
//				xhr.open("put", "/api/questions/"+ window.localStorage.questionId + "/comment/", true);
				console.log(" COMMENT URL: "+commentURL);
				xhr.open("put", commentURL, true);
				xhr.setRequestHeader("Content-Type", "application/json");
				xhr.setRequestHeader("Authorization", window.localStorage.accessToken);
				xhr.onreadystatechange = function() {
					if (xhr.readyState == 4) {
						$("#myModal").modal("hide");
						if(xhr.status == 200) {
							window.location.href = "/questions/" + window.localStorage.questionId;
						}
						else if(xhr.status == 401 || xhr.status == 502) {
							window.localStorage.clear();
							window.location.href = "/login/";
						}
						
					} 
				};
				xhr.send(JSON.stringify(edit_details_json));
			});
		});
		$("#myModal").modal();
	}
		
});