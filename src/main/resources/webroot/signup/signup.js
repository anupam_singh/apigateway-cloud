addEventListener("load", function() {
	console.log("SIGN UP PAGE LOADED");
	document.getElementById("signupButton").addEventListener("click", function(e) {
		document.getElementById("signup_alert_id").style.display = "none";
		var username = document.getElementById("userId").value;
		var password = document.getElementById("password_id").value;
		var name = document.getElementById("name_id").value;
		var city = document.getElementById("city_id").value;
//		if(window.localStorage.redirectURL )
		var signp_details_json = {
			"username" : username,
			"password" : password,
			"name" : name,
			"city" : city,
//			"return_url" : "/"
		};
		var xhr = new XMLHttpRequest();
		xhr.open("post", "/api/v1/users/", true);
		xhr.setRequestHeader("Content-Type", "application/json");
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				console.log("xhr.status: "+xhr.status);
				if(xhr.status == 200) {
//					var URL = xhr.getResponseHeader("location");
//					var token = xhr.getResponseHeader("Authorization");
//					window.localStorage.accessToken = token;
					window.location.href = "/login/";
				} else {
					console.log(" xhr responseText: "+xhr.responseText);
					document.getElementById("signup_alert_id").style.display = "block";
				}
			} 
		};
		console.log("SIGN UP LOADED login_details_json: "+JSON.stringify(signp_details_json));
		xhr.send(JSON.stringify(signp_details_json));
	})
});